package generator_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/kryabinin/gomigrator/pkg/generator"
)

func TestSql_Generate(t *testing.T) {
	sql := generator.NewSQL()
	defer func() {
		assert.NoError(t, os.Remove("test.sql"))
	}()

	err := sql.Generate("test", "./")
	assert.FileExists(t, "test.sql")
	assert.NoError(t, err)
}
