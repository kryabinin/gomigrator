package scanner

//go:generate mockery -case=underscore -output=. -inpkg -name=Scanner

import (
	"gitlab.com/kryabinin/gomigrator/pkg/migration"
)

type Scanner interface {
	Scan(path string) ([]migration.Migration, error)
}
